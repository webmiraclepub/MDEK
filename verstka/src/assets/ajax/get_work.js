var add_work = function(){
  $('#work .button-ajax button').fadeIn();
  var catID = $(this).attr('data-catID');
  if( $(this).hasClass('active') ){
  }else if( catID == 'all' ){
    $('#work .menu.category-navigation button.active').removeClass('active');
    $(this).addClass('active');
    $.ajax({
      method: "POST",
      url: 'assets/ajax/work.json',
    //url: ajax.url,
    //data: {
    //        "category":catID,
    //         } 
      success: function(data){
        var html = "";
        for( i = 0; i < 6; i++ ){
          var work = data.work[i];
          html += '<div class="cell" style="display:none;">'+
                    '<div class="card text-center" data-workid="'+work.workID+'" style="background-image: url(assets/img/'+work.image+')">'+
                      '<div class="card-section" data-equalizer-watch="work-card">'+
                        '<div class="hidden" data-equalizer-watch="content">'+
                          '<h4>'+work.title+'</h4>'+
                          '<p class="text-justify">'+
                            '<small>'+
                              work.content+
                            '</small>'+
                          '</p>'+
                        '</div>'+
                        '<div data-equalizer-watch="bottom">'+
                          '<p class="text-center">'+work.title+'</p>'+
                          '<div class="clear-both"></div>'+
                          '<p class="left none"><small>Срок: '+work.time+' '+work.date+'</small></p>'+
                          '<p class="right none"><small>Стоимость: '+work.price+' руб.</small></p>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
        }
        $('[data-equalizer="bottom"] .cell').fadeOut(500, function(){
          $('[data-equalizer="bottom"] .cell').remove();
          $('[data-equalizer="bottom"]').append(html);
          $('[data-equalizer="bottom"] .cell').fadeIn(500, function(){
            maxHeight( $('[data-equalizer-watch="content"]') );
            maxHeight( $('[data-equalizer-watch="bottom"]') );
            //Foundation.reInit($('[data-equalizer="content"]'));
            //if( Foundation.reInit($('[data-equalizer="content"]')) ){
              //var elem = new Foundation.Equalizer($('[data-equalizer="bottom"]'));
            //}
          });
        });
      }
    });
  }else{
    $('#work .menu.category-navigation button.active').removeClass('active');
    $(this).addClass('active');
    $.ajax({
      method: "POST",
      url: 'assets/ajax/work.json',
    //url: ajax.url,
    //data: {
    //        "category":catID,
    //         } 
      success: function(data){
        var html = "";
        var j = 0;
        var i = 0;
        while( j < 6 ){
          if( !data.work.indexOf(i) ){
            j = 6;
          }else if( data.work[i].category == catID ){
            var work = data.work[i];
            html += '<div class="cell" style="display:none;">'+
                      '<div class="card text-center" data-workid="'+work.workID+'" style="background-image: url(assets/img/'+work.image+')">'+
                        '<div class="card-section" data-equalizer-watch="work-card">'+
                          '<div class="hidden" data-equalizer-watch="content">'+
                            '<h4>'+work.title+'</h4>'+
                            '<p class="text-justify">'+
                              '<small>'+
                                work.content+
                              '</small>'+
                            '</p>'+
                          '</div>'+
                          '<div data-equalizer-watch="bottom">'+
                            '<p class="text-center">'+work.title+'</p>'+
                            '<div class="clear-both"></div>'+
                            '<p class="left none"><small>Срок: '+work.time+' '+work.date+'</small></p>'+
                            '<p class="right none"><small>Стоимость: '+work.price+' руб.</small></p>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
            j++;
          }
          i++;
        }
        $('[data-equalizer="bottom"] .cell').fadeOut(500, function(){
          $('[data-equalizer="bottom"] .cell').remove();
          $('[data-equalizer="bottom"]').append(html);
          $('[data-equalizer="bottom"] .cell').fadeIn(500, function(){
            maxHeight( $('[data-equalizer-watch="content"]') );
            maxHeight( $('[data-equalizer-watch="bottom"]') );
            //Foundation.reInit($('[data-equalizer="content"]'));
            //if( Foundation.reInit($('[data-equalizer="content"]')) ){
              //var elem = new Foundation.Equalizer($('[data-equalizer="bottom"]'));
            //}
          });
        });
      }
    });
  }
}
$('#work .menu.category-navigation button').click( add_work );


var add_new_work = function(){
  $('#work .button-ajax button').attr('disabled','');
  $('#work .button-ajax button').text('Загружаются...');
  var catID = $('#work .menu.category-navigation button.active').attr('data-catID');
  var works = [];
  var html= "";
  $('[data-equalizer="bottom"] [data-workid]').each(function(){
    works[ $(this).attr('data-workid') ] = $(this).attr('data-workid');
  });
  $.ajax({
      method: "POST",
      url: 'assets/ajax/work.json',
    //url: ajax.url,
    //data: {
    //        "category":catID,
    //         } 
      success: function(data){
        var html = "";
        var j = 0;
        var i = 1;
        if( catID == 'all' ){
          while( j < 3 ){
            if( data.work.length == i || data.work.length < i ){
              j = 3;
              $('#work .button-ajax button').fadeOut(400, function(){
                  $('#work .button-ajax button').removeAttr('disabled');
                  $('#work .button-ajax button').text('Показать еще');
                });
            }else if( data.work[i].workID in works ){
              i++;
            }else{
              console.log(data.work[i].workID);
              var work = data.work[i];
              html += '<div class="cell" style="display:none;">'+
                        '<div class="card text-center" data-workid="'+work.workID+'" style="background-image: url(assets/img/'+work.image+')">'+
                          '<div class="card-section" data-equalizer-watch="work-card">'+
                            '<div class="hidden" data-equalizer-watch="content">'+
                              '<h4>'+work.title+'</h4>'+
                              '<p class="text-justify">'+
                                '<small>'+
                                  work.content+
                                '</small>'+
                              '</p>'+
                            '</div>'+
                            '<div data-equalizer-watch="bottom">'+
                              '<p class="text-center">'+work.title+'</p>'+
                              '<div class="clear-both"></div>'+
                              '<p class="left none"><small>Срок: '+work.time+' '+work.date+'</small></p>'+
                              '<p class="right none"><small>Стоимость: '+work.price+' руб.</small></p>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>';
              j++;
              i++;
            }
          }
        }else{
          while( j < 3 ){
            if( data.work.length == i || data.work.length < i ){
              j = 3;
              $('#work .button-ajax button').fadeOut(400, function(){
                  $('#work .button-ajax button').removeAttr('disabled');
                  $('#work .button-ajax button').text('Показать еще');
                });
            }else if( data.work[i].workID in works ){
              var j = j;
            }else if( data.work[i].category == catID ){
              var work = data.work[i];
              html += '<div class="cell" style="display:none;">'+
                        '<div class="card text-center" data-workid="'+work.workID+'" style="background-image: url(assets/img/'+work.image+')">'+
                          '<div class="card-section" data-equalizer-watch="work-card">'+
                            '<div class="hidden" data-equalizer-watch="content">'+
                              '<h4>'+work.title+'</h4>'+
                              '<p class="text-justify">'+
                                '<small>'+
                                  work.content+
                                '</small>'+
                              '</p>'+
                            '</div>'+
                            '<div data-equalizer-watch="bottom">'+
                              '<p class="text-center">'+work.title+'</p>'+
                              '<div class="clear-both"></div>'+
                              '<p class="left none"><small>Срок: '+work.time+' '+work.date+'</small></p>'+
                              '<p class="right none"><small>Стоимость: '+work.price+' руб.</small></p>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>';
              j++;
            }
            i++;
          }
        }

        $('[data-equalizer="bottom"]').append(html);
        $('[data-equalizer="bottom"] .cell').fadeIn(500, function(){
          maxHeight( $('[data-equalizer-watch="content"]') );
          maxHeight( $('[data-equalizer-watch="bottom"]') );
          //Foundation.reInit($('[data-equalizer="content"]'));
            //if( Foundation.reInit($('[data-equalizer="content"]')) ){
              //var elem = new Foundation.Equalizer($('[data-equalizer="bottom"]'));
            //}
        });
        $('#work .button-ajax button').removeAttr('disabled');
        $('#work .button-ajax button').text('Показать еще');
      }
    });
  }
$('#work .button-ajax button').click( add_new_work );

function maxHeight(arr){
  var h = $(arr[0]).height();
  for( i=1; i < arr.length; i++ ){
    if( h < $(arr[i]).height() ){
      h = $(arr[i]).height();
    }
  }
  
  for( i=0; i < arr.length; i++ ){
    $(arr[i]).height(h);
  }  
}