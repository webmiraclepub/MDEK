## Проект реализуется через Zurb template (Foundation project)

Предварительные установки зависимостей:
- nodejs
- foundation-cli

## Начало проекта

В корневой папке проекта инициализируем Foundation проект `foundation new`, имя проекта `verstka`, temlate `Zurb Foundation`, создаст папку `./verstka` в которой будет содержаться front end проекта, для запуска и работы с проектом прописываем команду `npm start`
Создаем `.gitignore` в папку `verstka` проекта содержащий
```
dist
etc
node_modules
CHANGELOG.md
config.yml
gulpfile.babel.js
package.json
.babelrc
.bowerrc
```

## Создаем папку `demo` в корне проекта в котором будут храниться все исходники psd, и правки заказчика

## Создаем папку WP, в которую распаковываем WordPress

В директории `WP` создаем `.gitignore` содержащий:

```
wp-admin
wp-includes
wp-content/themes/twentyfifteen
wp-content/themes/twentyseventeen
wp-content/themes/twentysixteen
license.txt
readme.html
*.php
```

```
Доступ к удаленному WP:
hardnig.ga
dnovikov32
waeserreak
```
